from django.contrib import admin

from .models import UserOwnedItem,Item,ActivityLog,Partner,User,PositionAndSalary


class Admin(admin.ModelAdmin):
    pass


admin.site.register(User)
