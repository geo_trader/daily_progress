from django import forms
from uuid import UUID


class UserIDForm(forms.Form):
    user_id = forms.UUIDField(
        label='Input your ID(empty starts new game)',
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'e.g:f9375ca2-b6e1-4d9c-ba60-32be266bef4b'}
        )
    )

    def clean_user_id(self):
        user_id = self.cleaned_data['user_id']
        try:
            UUID(user_id, version=4)
        except ValueError:
            raise forms.ValidationError(
                'input correct UUID format like f9375ca2-b6e1-4d9c-ba60-32be266bef4b'
            )
