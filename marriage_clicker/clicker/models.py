from django.db import models


class User(models.Model):
    user_uuid = models.UUIDField(verbose_name='user_uuid')
    days = models.IntegerField(verbose_name='days', default=0)
    wallet = models.IntegerField(verbose_name='wallet', default=0)
    user_modify_date = models.DateField(auto_now=True, blank=True, verbose_name='user_modify_date')
    user_create_date = models.DateField(auto_now_add=True, blank=True, verbose_name='user_create_date')

    def __str__(self):
        return str(self.user_uuid)


class Item(models.Model):
    item_name = models.TextField(verbose_name='item_name')
    item_price = models.IntegerField(verbose_name='item_price')
    charm_point = models.IntegerField(verbose_name='charm_point')

    def __str__(self):
        return self.item_name


class UserOwnedItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    quantity = models.IntegerField(default=0, verbose_name='quantity')
    item_get_date = models.DateField(auto_now=True, blank=True, verbose_name='item_get_date')


class ActivityLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    log_date = models.DateField(auto_now_add=True, blank=True, verbose_name='log_date')
    text = models.TextField(verbose_name='text')

    def __str__(self):
        return self.text


class PositionAndSalary(models.Model):
    position = models.TextField(verbose_name='position')
    salary = models.IntegerField(verbose_name='salary')
    difficult = models.IntegerField(verbose_name='difficult')

    def __str__(self):
        return self.position


class Partner(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    position = models.ForeignKey(PositionAndSalary, on_delete=models.PROTECT, default=1)
    promotion_date = models.DateField(auto_now=True, blank=True, verbose_name='promotion_date')

    def __str__(self):
        return str(self.user)


class GameParameter(models.Model):
    salary_interval_days = models.IntegerField()
    promotion_event_interval_days = models.IntegerField()
    target_amount_of_money = models.IntegerField()
