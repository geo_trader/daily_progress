from django.urls import path
from . import views
from django.views.generic import RedirectView

app_name = 'clicker'
urlpatterns = [
    path('', RedirectView.as_view(url='/clicker/welcome/'), name='top'),
    path('welcome/', views.WelcomeView.as_view(), name='welcome'),
    path('game/<uuid:user_id>/', views.GameView.as_view(), name='game'),
    path('clear/<uuid:user_id>/', views.ClearView.as_view(), name='clear')
]
