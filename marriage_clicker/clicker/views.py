from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from .forms import UserIDForm
from uuid import uuid4
from django.db.models import Sum, F
from clicker.models import User, Item, UserOwnedItem, ActivityLog, Partner, PositionAndSalary


class WelcomeView(View):
    form = UserIDForm

    def get(self, request, *args, **kwargs):
        context = {
            'form': self.form,
        }
        return render(request, 'clicker/start.html', context)

    def post(self, request, *args, **kwargs):
        uuid = request.POST.get('user_id')

        if not uuid:
            uuid = str(uuid4())

        return HttpResponseRedirect('../game/' + uuid + '/')


class AbstractGameView(View):
    salary_interval_days = 1
    promotion_event_interval_days = 3
    target_amount_of_money = 2000000

    def get_stats(self, user, partner):
        owned_items = UserOwnedItem.objects.filter(user_id=user)
        total_charm = self.calc_total_charm(owned_items=owned_items)
        stats = {'stats': {
            'Your ID': str(user.user_uuid),
            'Days': user.days,
            'Wallet': user.wallet,
            'Charm': total_charm,
            'Partner position in company': partner.position,
            'Partner Salary': partner.position.salary,
            'Remaining days for next promotion event':
                self.promotion_event_interval_days - (user.days % self.promotion_event_interval_days),
            'Remaining money for target reach': self.target_amount_of_money - user.wallet,
        }}

        stats.update({
            'owned_items': UserOwnedItem.objects.filter(user_id=user).values('quantity', 'item_id__item_name')
        })

        return stats

    def calc_total_charm(self,owned_items):
        total_charm = owned_items.aggregate(total_charm=Sum(F('item_id__charm_point')*F('quantity')))['total_charm']

        if total_charm:
            return total_charm
        else:
            return 0


class GameView(AbstractGameView):

    def get(self, request, user_id, *args, **kwargs):
        user = User.objects.filter(user_uuid=user_id).first()
        if not user:
            user = User(user_uuid=user_id)
            user.save()
            partner = Partner(user_id=user.pk)
            partner.save()
        else:
            partner = Partner.objects.get(user_id=user)

        if 'restart_game' in request.GET:
            uuid = str(uuid4())
            return HttpResponseRedirect('../' + uuid + '/')

        elif 'buy_item' in request.GET:
            item_name = request.GET['buy_item']
            item = get_object_or_404(Item, item_name=item_name)

            if user.wallet >= item.item_price:
                bought_item = UserOwnedItem.objects.filter(user_id=user, item_id=item).first()

                if not bought_item:
                    bought_item = UserOwnedItem(user_id=user.pk, item_id=item.pk)
                bought_item.quantity += 1
                bought_item.save()

                user.wallet -= item.item_price
                user.save()
                self.write_activity_stream(user, 'you spend {price} and bought shoes.'.format(price=item.item_price))
            else:
                self.write_activity_stream(user, 'not enough money. you should request more work to your Partner.')

        elif 'next_day' in request.GET:
            user.days += 1
            if user.days % self.salary_interval_days == 0:
                salary = PositionAndSalary.objects.filter(position=partner.position).first().salary
                user.wallet += salary
                self.write_activity_stream(user, 'you earned {salary} by salary'.format(salary=salary))

                if user.wallet >= self.target_amount_of_money:
                    return HttpResponseRedirect('../../clear/' + str(user_id) + '/')

            if user.days % self.promotion_event_interval_days == 0:
                before_position = str(partner.position)
                new_position = self.promotion_event(user=user, partner=partner)

                if before_position == new_position:
                    text = 'nothing promotion. you need more charm...'
                    self.write_activity_stream(user=user, text=text)
                else:
                    text = 'you have got promotion! {before_job} to {new_job}.'  \
                        .format(before_job=before_position, new_job=new_position)
                    self.write_activity_stream(user=user, text=text)
            user.save()

        context = {
            'shop_items': Item.objects.values('item_name', 'item_price', 'charm_point'),
        }
        context.update(self.get_stats(user=user, partner=partner))
        context.update(self.get_activity_stream(user=user))

        return render(request, 'clicker/game.html', context)

    def promotion_event(self, user, partner):
        self.write_activity_stream(user, 'Promotion event day!.')
        owned_items = UserOwnedItem.objects.filter(user_id=user)
        total_charm = self.calc_total_charm(owned_items=owned_items)
        if not total_charm:
            total_charm = 0

        cleared_job = PositionAndSalary.objects.filter(difficult__lte=total_charm)
        new_position = cleared_job.order_by('difficult').reverse().first()

        partner.position = new_position
        partner.save()

        return str(new_position)


    def write_activity_stream(self, user, text):
        ActivityLog.objects.create(user_id=user.pk, text=text)

    def get_activity_stream(self, user):
        tmp = ActivityLog.objects.filter(user_id=user).order_by('log_date').reverse().values('log_date', 'text')
        activity_streams = {'activity_streams': tmp}
        return activity_streams


class ClearView(AbstractGameView):
    def get(self, request, user_id, *args, **kwargs):
        user = User.objects.filter(user_uuid=user_id).first()
        partner = Partner.objects.get(user_id=user)

        context = {
            'stats': self.get_stats(user_id=user, partner=partner)
        }
        context.update()

        return render(request, 'clicker/clear.html', context)

